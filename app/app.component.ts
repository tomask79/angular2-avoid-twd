import {Component} from 'angular2/core';
import {PersonEditorComponent} from './person-editor.component';
import {Person} from './Person';

@Component({
    selector: 'my-app',
    template: `<div>Person name: {{person.name}}</div> 
               <div>Person surname: {{person.surname}}</div> 
               <person-editor [person]="person"></person-editor>
              `,
    directives: [PersonEditorComponent]
})
export class AppComponent { 
    private person = new Person('SomeFirstName', 'SomeSurname');
}