import {Component, Input} from 'angular2/core';
import { Person } from './Person';

@Component({
    selector: 'person-editor',
    template: `Name: <input type=text [(ngModel)]="person.name" />
               Surname: <input type=text [(ngModel)]="person.surname" />
              `
})

export class PersonEditorComponent {
    @Input() person: Person;
}