# Writing applications in Angular 2 [part 14] #

## Avoiding of two way data-binding in Angular 2 ##

In this post, let's show something easy, but which is not that much obvious from the Angular 2 docs, which is still not complete. If you want your child component to change the input and return it back to it's parent, then one way to solve this is to use **two-way data binding** like I showed here in [part 6](https://bitbucket.org/tomask79/angular2-two-way-databinding).  

Anyway, you can get this effect with using **Input decorator** only. Yes, Angular 2 says about the Input just: 'Declares a data-bound input property.' But apparently every input to child component, if it's the primitive, is shadowed in the child component into completely new property with the same name or declared name. Let's show an example:

(Parent) app.component.ts     
----------------

```
import {Component} from 'angular2/core';
import {PersonEditorComponent} from './person-editor.component';
import {Person} from './Person';

@Component({
    selector: 'my-app',
    template: `<div>Person name: {{person.name}}</div> 
               <div>Person surname: {{person.surname}}</div> 
               <person-editor [person]="person"></person-editor>
              `,
    directives: [PersonEditorComponent]
})
export class AppComponent { 
    private person = new Person('SomeFirstName', 'SomeSurname');
}
```
(child) person-editor.component

```
import {Component, Input} from 'angular2/core';
import { Person } from './Person';

@Component({
    selector: 'person-editor',
    template: `Name: <input type=text [(ngModel)]="person.name" />
               Surname: <input type=text [(ngModel)]="person.surname" />
              `
})

export class PersonEditorComponent {
    @Input() person: Person;
}
```
Now the most important part, **since we passed an object into child component, then every change the child component will make to Input's person object will be immediately seen in the parent component**, without any using of @Output decorator...If you change the input for any primitive data type, the connection between the parent and child will be broken....Test it for yourself.

## Testing the demo ##

* git clone <this repo>
* npm install
* npm start
* visit localhost:3000